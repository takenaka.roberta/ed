"""
Casos de Uso de Lista ligada:
    Serve para alocar e desalocar memória conforme a demanda
    Os itens da lista são relacionadoligados,
    um item "sabe" qual item é seu posterior.
    Pode-se implementar uma Fila de forma dinâmica.
"""

from ed.lista_encadeada import ListaLigada


def usar_lista_ligada():

    print("Inserção no início")
    lista_ligada = ListaLigada()
    lista_ligada.inserir_no_inicio("Item 1")
    lista_ligada.inserir_no_inicio("Item 2")
    print("Resultado esperado é: Item 2, Item 1")
    lista_ligada.imprimir()
    print("")
    print("Inserção no fim")
    lista_ligada.inserir_no_fim("Item 3")
    lista_ligada.inserir_no_fim("Item 4")
    print("Resultado esperado é: Item 2, Item 1, Item 3, Item 4")
    lista_ligada.imprimir()

    print("\nInserção no meio: na posição 2")
    lista_ligada.inserir(2, "A")
    print("Resultado esperado é: Item 2, Item 1, A, Item 3, Item 4")
    lista_ligada.imprimir()

    print("\nRemoção do início")
    removido = lista_ligada.remover_do_inicio()
    print(removido.conteudo)
    print("Resultado esperado é: Item 1, A, Item 3, Item 4")
    lista_ligada.imprimir()

    print("\nRemoção do início")
    removido = lista_ligada.remover(2)
    print(removido.conteudo)
    print("Esperado é Item 3")
    print("Resultado esperado é: Item 1, A, Item 4")
    lista_ligada.imprimir()
    removido = lista_ligada.remover(2)
    print(removido.conteudo)
    print("Esperado é Item 4")
    print("Resultado esperado é: Item 1, A")
    lista_ligada.imprimir()


usar_lista_ligada()
