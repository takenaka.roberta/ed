import unittest
from ed.lista_duplamente_ligada import ListaDuplamenteLigada


class TestListaDuplamenteLigada(unittest.TestCase):
    """Testes de lista encadeada."""

    def setUp(self):
        """Inicializa os testes de lista encadeada."""
        self.lista = ListaDuplamenteLigada()

    def test_localizar_em_lista_vazia(self):
        atual = self.lista.localizar(0)
        self.assertEqual(atual, None)

    def test_localizar_em_posicao_invalida(self):
        self.assertRaises(ValueError, self.lista.localizar, 100)

    def test_inserir_em_posicao_invalida(self):
        self.assertRaises(ValueError, self.lista.inserir, 100, "item n")

    def test_inserir_em_lista_vazia(self):
        self.lista.inserir(0, "elemento 0")
        self.assertEqual(self.lista.inicio.conteudo, "elemento 0")
        self.assertEqual(self.lista.fim.conteudo, "elemento 0")
        self.assertEqual(self.lista.quantidade, 1)

    def test_localizar_posicao_0_em_lista_com_1_elemento(self):
        self.lista.inserir(0, "elemento 0")
        atual = self.lista.localizar(0)
        self.assertEqual(atual.anterior, None)
        self.assertEqual(atual.conteudo, "elemento 0")

    def test_localizar_posicao_1_em_lista_com_1_elemento(self):
        self.lista.inserir(0, "elemento 0")
        atual = self.lista.localizar(1)
        self.assertEqual(atual, None)
        self.assertEqual(self.lista.inicio.conteudo, "elemento 0")

    def test_inserir_2_elementos_na_sequencia(self):
        self.lista.inserir(0, "elemento 0")
        atual = self.lista.localizar(0)
        self.assertEqual(atual.anterior, None)
        self.assertEqual(atual.conteudo, "elemento 0")

        self.lista.inserir(1, "elemento 1")
        atual = self.lista.localizar(1)
        self.assertEqual(atual.anterior.conteudo, "elemento 0")
        self.assertEqual(atual.conteudo, "elemento 1")
        self.assertEqual(self.lista.fim.conteudo, "elemento 1")

    def test_inserir_1_entre_dois(self):
        self.lista.inserir(0, "elemento 0")
        atual = self.lista.localizar(0)
        self.assertEqual(atual.anterior, None)
        self.assertEqual(atual.conteudo, "elemento 0")

        self.lista.inserir(1, "elemento 1")
        atual = self.lista.localizar(1)
        self.assertEqual(atual.anterior.conteudo, "elemento 0")
        self.assertEqual(atual.conteudo, "elemento 1")

        self.lista.inserir(1, "elemento entre 1 e 2")
        atual = self.lista.localizar(1)
        self.assertEqual(atual.anterior.conteudo, "elemento 0")
        self.assertEqual(atual.conteudo, "elemento entre 1 e 2")
        self.assertEqual(atual.proximo.conteudo, "elemento 1")
        # self.assertEqual(
        #     self.lista.elementos, ["elemento 0", "elemento entre 1 e 2", "elemento 1"]
        # )
        self.assertEqual(self.lista.conteudo(0), "elemento 0")
        self.assertEqual(self.lista.conteudo(1), "elemento entre 1 e 2")
        self.assertEqual(self.lista.conteudo(2), "elemento 1")

    def test_inserir_elemento_no_inicio_em_lista_nao_vazia(self):
        self.lista.inserir(0, "elemento 0")
        atual = self.lista.localizar(0)
        self.assertEqual(atual.anterior, None)
        self.assertEqual(atual.conteudo, "elemento 0")
        self.assertEqual(self.lista.inicio.conteudo, "elemento 0")
        self.assertEqual(self.lista.fim.conteudo, "elemento 0")

        self.lista.inserir(1, "elemento 1")
        atual = self.lista.localizar(1)
        self.assertEqual(atual.anterior.conteudo, "elemento 0")
        self.assertEqual(atual.conteudo, "elemento 1")

        self.lista.inserir(0, "elemento no inicio")
        atual = self.lista.localizar(0)
        self.assertEqual(atual.anterior, None)
        self.assertEqual(atual.conteudo, "elemento no inicio")
        self.assertEqual(atual.proximo.conteudo, "elemento 0")
        self.assertEqual(self.lista.conteudo(0), "elemento no inicio")
        self.assertEqual(self.lista.conteudo(1), "elemento 0")
        self.assertEqual(self.lista.conteudo(2), "elemento 1")
        # self.assertEqual(
        #     self.lista.elementos, ["elemento no inicio", "elemento 0", "elemento 1"]
        # )

    def test_remover_elemento_do_inicio(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        self.lista.remover_elemento("A")
        # self.assertEqual(self.lista.elementos, ["B", "C"])
        self.assertEqual(self.lista.conteudo(0), "B")
        self.assertEqual(self.lista.conteudo(1), "C")
        self.assertEqual(self.lista.inicio.conteudo, "B")

    def test_remover_elemento_do_fim(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        self.lista.remover_elemento("C")
        self.assertEqual(self.lista.fim.conteudo, "B")
        self.assertEqual(self.lista.conteudo(0), "A")
        self.assertEqual(self.lista.conteudo(1), "B")
        # self.assertEqual(self.lista.elementos, ["A", "B"])

    def test_remover_elemento_do_meio(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        self.lista.remover_elemento("B")
        # self.assertEqual(self.lista.elementos, ["A", "C"])
        self.assertEqual(self.lista.conteudo(0), "A")
        self.assertEqual(self.lista.conteudo(1), "C")
        self.assertEqual(self.lista.fim.conteudo, "C")
        self.assertEqual(self.lista.inicio.conteudo, "A")

    def test_remover_da_posicao_inicial(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        elemento = self.lista.remover_elemento("A")
        self.assertEqual(elemento.conteudo, "A")
        self.assertEqual(elemento.proximo, None)
        self.assertEqual(self.lista.fim.conteudo, "C")
        self.assertEqual(self.lista.inicio.conteudo, "B")
        # self.assertEqual(self.lista.elementos, ["B", "C"])
        self.assertEqual(self.lista.conteudo(0), "B")
        self.assertEqual(self.lista.conteudo(1), "C")

    def test_remover_da_posicao_final(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        elemento = self.lista.remover_elemento("C")
        self.assertEqual(elemento.conteudo, "C")
        self.assertEqual(elemento.proximo, None)
        self.assertEqual(self.lista.fim.conteudo, "B")
        self.assertEqual(self.lista.inicio.conteudo, "A")
        # self.assertEqual(self.lista.elementos, ["A", "B"])
        self.assertEqual(self.lista.conteudo(0), "A")
        self.assertEqual(self.lista.conteudo(1), "B")

    def test_remover_da_posicao_1(self):
        self.lista.inserir(0, "A")
        self.lista.inserir(1, "B")
        self.lista.inserir(2, "C")
        elemento = self.lista.remover_elemento("B")
        self.assertEqual(elemento.conteudo, "B")
        self.assertEqual(elemento.proximo, None)
        self.assertEqual(self.lista.fim.conteudo, "C")
        self.assertEqual(self.lista.quantidade, 2)
        self.assertEqual(self.lista.inicio.conteudo, "A")
        # self.assertEqual(self.lista.elementos, ["A", "C"])
        self.assertEqual(self.lista.conteudo(0), "A")
        self.assertEqual(self.lista.conteudo(1), "C")
