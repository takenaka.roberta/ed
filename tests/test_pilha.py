import unittest
from unittest import mock

from ed import pilha
from ed.pilha import Pilha as PilhaED
from ed.pilha_pythonica import Pilha as PilhaPy
from ed.pilha_com_lista import Pilha as PilhaL


class TestPilha(unittest.TestCase):
    def setUp(self):
        self.pilha_p = PilhaPy()
        self.pilha_ed = PilhaED(str)
        self.pilha_l = PilhaL()

    def test_empilhar(self):
        for i, p in enumerate([self.pilha_p, self.pilha_ed, self.pilha_l]):
            with self.subTest(i):
                p.empilhar("Home Page")
                self.assertEqual(p.topo, "Home Page")
                p.empilhar("Página nível 1")
                self.assertEqual(p.topo, "Página nível 1")
                p.empilhar("Página nível 2")
                self.assertEqual(p.topo, "Página nível 2")

    def test_desempilhar(self):
        for i, p in enumerate([self.pilha_p, self.pilha_ed, self.pilha_l]):
            with self.subTest(i):
                p.empilhar("Home Page")
                self.assertEqual(p.topo, "Home Page")
                p.empilhar("Página nível 1")
                self.assertEqual(p.topo, "Página nível 1")
                p.empilhar("Página nível 2")
                self.assertEqual(p.topo, "Página nível 2")
                topo = p.desempilhar()
                self.assertEqual(topo, "Página nível 2")
                self.assertEqual(p.topo, "Página nível 1")
                topo = p.desempilhar()
                self.assertEqual(topo, "Página nível 1")
                topo = p.desempilhar()
                self.assertEqual(topo, "Home Page")

    def test_alocar(self):
        PilhaED.ALOCADOS = 3
        p = PilhaED(int)
        self.assertEqual([int(), int(), int()], p._elementos)
        self.assertEqual(p._alocados, 3)

    def test_alocar_mais(self):
        PilhaED.ALOCADOS = 3
        p = PilhaED(int)
        self.assertEqual([int(), int(), int()], p._elementos)
        self.assertEqual(p._alocados, 3)
        p.empilhar(1)
        p.empilhar(2)
        p.empilhar(3)
        p.empilhar(4)
        self.assertEqual([1, 2, 3, 4, int(), int()], p._elementos)
        self.assertEqual(p._alocados, 6)

    def test_alocar_dobro_3_6_12_24(self):
        PilhaED.ALOCADOS = 3
        p = PilhaED(int)
        self.assertEqual([int(), int(), int()], p._elementos)
        self.assertEqual(p._alocados, 3)
        for i in range(1, 4):
            p.empilhar(i)
        self.assertEqual(p._alocados, 3)
        p.empilhar(4)
        self.assertEqual(p._alocados, 6)
        for i in range(1, 9):
            p.empilhar(4 + i)
        self.assertEqual(p._alocados, 12)
        p.empilhar(13)
        self.assertEqual(p._alocados, 24)

    def test_desalocar(self):
        PilhaED.ALOCADOS = 3
        p = PilhaED(int)
        self.assertEqual([int(), int(), int()], p._elementos)
        self.assertEqual(p._alocados, 3)
        for i in range(1, 4):
            p.empilhar(i)
        self.assertEqual(p._alocados, 3)
        p.empilhar(4)
        self.assertEqual(p._alocados, 6)
        for i in range(1, 9):
            p.empilhar(4 + i)
        self.assertEqual(p._alocados, 12)
        self.assertEqual(p._elementos, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        p.empilhar(13)
        self.assertEqual(
            p._elementos, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] + [0] * 11
        )
        self.assertEqual(p._alocados, 24)
        p.desempilhar()
        self.assertEqual(p._alocados, 12)
        self.assertEqual(p._elementos, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        for i in range(1, 6):
            p.desempilhar()
        self.assertEqual(p._alocados, 12)
        self.assertEqual(p._elementos, [1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 0])
        p.desempilhar()
        self.assertEqual(p._alocados, 6)
        self.assertEqual(p._elementos, [1, 2, 3, 4, 5, 6])
        p.desempilhar()
        p.desempilhar()
        p.desempilhar()
        self.assertEqual(p._alocados, 3)
        self.assertEqual(p._elementos, [1, 2, 3])
        p.desempilhar()
        self.assertEqual(p._elementos, [1, 2, 0])
        p.desempilhar()
        self.assertEqual(p._elementos, [1, 0, 0])
        p.desempilhar()
        self.assertEqual(p._topo, -1)
        self.assertEqual(p._elementos, [])
        self.assertEqual(p._alocados, 0)
