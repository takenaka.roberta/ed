import unittest

from ed.arvore_binaria import ArvoreBinaria


class TestArvoreBinaria(unittest.TestCase):
    def test_inserir_elementos(self):
        arvore = ArvoreBinaria()
        arvore.inserir("m")
        self.assertEqual(arvore.raiz.conteudo, "m")

        arvore.inserir("b")
        self.assertEqual(arvore.raiz.esquerda.conteudo, "b")

        arvore.inserir("n")
        self.assertEqual(arvore.raiz.direita.conteudo, "n")

        arvore.inserir("a")
        self.assertEqual(arvore.raiz.esquerda.esquerda.conteudo, "a")

        arvore.inserir("l")
        self.assertEqual(arvore.raiz.esquerda.direita.conteudo, "l")

    def test_remover(self):
        arvore = ArvoreBinaria()
        numeros = [50, 20, 65, 10, 21, 23, 70, 63, 80, 55]
        expected = {
            "nodo": 50,
            "menores": {
                "nodo": 20,
                "menores": {
                    "nodo": 10,
                },
                "maiores": {
                    "nodo": 21,
                    "menores": {},
                    "maiores": {
                        "nodo": 23,
                        "menores": {},
                        "maiores": {},
                    },

                }
                },
            "maiores": {
                "nodo": 65,
                "menores": {
                    "nodo": 63,
                    "menores": {
                        "nodo": 55,
                    }
                },
                "maiores": {
                    "nodo": 70,
                    "menores": {},
                    "maiores": {
                        "nodo": 80,
                    }
                },
                }
            }
        self.assertEqual(arvore.raiz.info, expected)

        novos_numeros = [50, 65, 10, 23, 80, 55]
        for item in [20, 21, 70, 63]:
            arvore.remover(item)
        self.assertEqual(arvore.elementos, sorted(novos_numeros))
