"""
Casos de Uso de Pilha:
    Funções recursivas em compiladores;
    Mecanismo de desfazer/refazer dos editores de texto;
    Navegação entre páginas Web;
"""

from ed.pilha import Pilha as PilhaED
from ed.pilha_com_lista import Pilha as PilhaLD
from ed.pilha_pythonica import Pilha as PilhaPy


def faz_algumas_tarefas_com_pilha(pilha):
    pilha.empilhar("Home")
    print(pilha.topo == "Home")

    pilha.empilhar("Nível 1")
    print(pilha.topo == "Nível 1")

    pilha.empilhar("Nível 2")
    print(pilha.topo == "Nível 2")

    pilha.empilhar("Fale conosco")
    pilha.empilhar("Resposta do Formulário de Fale conosco")
    pagina = pilha.desempilhar()

    print(pagina == "Resposta do Formulário de Fale conosco")
    print(pilha.topo == "Fale conosco")
    print(pilha.is_empty)

    while not pilha.is_empty:
        print(pilha.desempilhar())


def main():
    for pilha in [PilhaED(str), PilhaLD(), PilhaPy()]:
        faz_algumas_tarefas_com_pilha(pilha)


main()
