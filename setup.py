# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='ed',
    version='0.1.0',
    description='Estrutura de Dados',
    long_description=readme,
    author='Roberta Takenaka',
    author_email='takenaka.roberta@gmail.com',
    url='https://gitlab.com/takenakaroberta/ed',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
