from ed.arvore import Nodo, Arvore
import json


def cria_departamento(departamento):
    return Nodo({"departamento": departamento})


def cria_cargo(cargo):
    return Nodo({"cargo": cargo})


def cria_pessoa(nome):
    return Nodo({"pessoa": nome})


def cria_dados_completos(nome, cargo):
    return Nodo({"pessoa": nome, "cargo": cargo})


def main():

    presidente = cria_cargo("Presidente")
    dep_hotelaria = cria_cargo("Gerente de Hotelaria")
    dep_restaurante = cria_cargo("Gerente de Restaurante")
    dep_escritorio = cria_cargo("Gerente de Escritório")
    cozinha = cria_departamento("Cozinha")
    salao = cria_departamento("Salão")
    limpeza = cria_departamento("Limpeza")
    manut = cria_departamento("Manutenção")
    recepcao = cria_departamento("Recepção")

    arvore = Arvore()
    arvore.raiz = presidente

    presidente.inserir_filho(dep_hotelaria)
    presidente.inserir_filho(dep_restaurante)
    presidente.inserir_filho(dep_escritorio)

    dep_hotelaria.inserir_filho(limpeza)
    dep_hotelaria.inserir_filho(manut)

    dep_restaurante.inserir_filho(cozinha)
    dep_restaurante.inserir_filho(salao)

    dep_escritorio.inserir_filho(recepcao)

    recepcao.inserir_filho(cria_pessoa("Miguel"))
    recepcao.inserir_filho(cria_pessoa("Lívia"))
    recepcao.inserir_filho(cria_pessoa("Sophia"))

    salao.inserir_filho(cria_pessoa('Davi'))
    salao.inserir_filho(cria_pessoa('Alice'))
    salao.inserir_filho(cria_pessoa('Isabela'))
    salao.inserir_filho(cria_pessoa('Helena'))

    cozinha.inserir_filho(cria_dados_completos("Pedro", "chefe"))
    cozinha.inserir_filho(cria_pessoa("Joaquim"))
    cozinha.inserir_filho(cria_pessoa("Laura"))
    cozinha.inserir_filho(cria_pessoa("Ana Laura"))

    limpeza.inserir_filho(cria_pessoa("Theo"))
    limpeza.inserir_filho(cria_pessoa("Liam"))
    limpeza.inserir_filho(cria_pessoa("Luna"))
    limpeza.inserir_filho(cria_pessoa("Manuela"))
    limpeza.inserir_filho(cria_pessoa("Lorenzo"))

    manut.inserir_filho(cria_pessoa("Evelyn"))
    manut.inserir_filho(cria_pessoa("Thomas"))
    manut.inserir_filho(cria_pessoa("Luna"))
    manut.inserir_filho(cria_pessoa("Jack"))
    manut.inserir_filho(cria_pessoa("Lucca"))

    app_json = json.dumps(arvore.info)
    with open("arvore.json", "w") as f:
        f.write(app_json)

    # Contratamos, agora há um nome para o Gerente de Hotelaria
    dep_hotelaria.dados.update({"pessoa": "Francisco"})

    # Uma pessoa da recepcao pediu demissao
    pessoa = recepcao.filhos.conteudo(3)
    recepcao.remover_filho(pessoa)
    app_json = json.dumps(arvore.info)
    with open("arvore.alterado.json", "w") as f:
        f.write(app_json)

main()
