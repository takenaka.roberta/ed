"""
Casos de Uso de Lista duplamente ligada:
    Serve para alocar e desalocar memória conforme a demanda
    Os itens da lista são relacionados, ligados,
    um item "sabe" qual item é seu anterior e seu posterior.
    Pode-se implementar uma Pilha de forma dinâmica.
"""

from ed.lista_duplamente_ligada import ListaDuplamenteLigada


def faz_algumas_tarefas_com_lista_duplamente_ligada():
    lista = ListaDuplamenteLigada()
    lista.inserir_no_inicio("Zebra")
    lista.imprimir()

    lista.inserir_no_inicio("Leão")
    lista.imprimir()

    lista.inserir_no_fim("Morsa")
    lista.imprimir()

    lista.inserir(2, "Arara")
    lista.imprimir()
    lista.inserir(2, "Aranha")
    lista.imprimir()
    lista.inserir(4, "Tubarão")
    lista.imprimir()
    lista.inserir(2, "Iguana")
    lista.imprimir()
    lista.inserir(1, "Foca")
    lista.imprimir()
    lista.inserir(2, "Baleia")

    lista.remover_do_fim()
    lista.imprimir()

    lista.remover_do_inicio()
    lista.imprimir()

    lista.remover_elemento("Morsa")
    lista.imprimir()

    lista.remover(5)
    lista.imprimir()


faz_algumas_tarefas_com_lista_duplamente_ligada()
