"""Pilha  - Estrutura de Dados - conceito - desconsiderando as características de Python."""


class Pilha:

    ALOCADOS = 100

    def __init__(self, tipo):
        self._tipo = tipo
        self._elementos = list()
        self._alocados = 0
        self.__topo = -1
        self._alocar()

    def _alocar(self):
        q = self._alocados
        self._alocados *= 2
        if self._alocados == 0:
            q = self.ALOCADOS
            self._alocados = q
        self._elementos += [self._tipo()] * q

    def _desalocar(self):
        qtd_a_desalocar = self._alocados // 2
        q = qtd_a_desalocar
        if qtd_a_desalocar < self.ALOCADOS:
            qtd_a_desalocar = self.ALOCADOS
            q = 0
        for i in range(0, qtd_a_desalocar):
            self._elementos.pop()
        self._alocados = q

    def empilhar(self, elemento):
        self._topo = 1
        self._elementos[self._topo] = elemento

    def _pode_desalocar(self):
        indicador_para_desalocar = self._alocados // 2
        if indicador_para_desalocar < self.ALOCADOS:
            indicador_para_desalocar = 0
        return self._topo < indicador_para_desalocar

    def desempilhar(self):
        if self._topo >= 0:
            topo = self._elementos[self._topo]
            self._elementos[self._topo] = self._tipo()
            self._topo = -1
            return topo

    @property
    def _topo(self):
        return self.__topo

    @_topo.setter
    def _topo(self, valor):
        if valor > 0:
            self.__topo += 1
            if self._topo == self._alocados:
                self._alocar()
        else:
            self.__topo -= 1
            if self._pode_desalocar():
                self._desalocar()

    @property
    def is_empty(self):
        return self._topo == -1

    @property
    def topo(self):
        if self._topo >= 0:
            return self._elementos[self._topo]
