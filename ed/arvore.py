from ed.lista_duplamente_ligada import ListaDuplamenteLigada


    def __init__(self, dados):
        self.dados = dados
        self.pai = None
        self.filhos = ListaDuplamenteLigada()

    @property
    def irmaos(self):
        if not self.pai:
            return
        _irmaos = ListaDuplamenteLigada()
        for atual in self.pai.filhos:
            if atual is not nodo:
                _irmaos.inserir_no_fim(atual)
        return _irmaos

    def inserir_filho(self, filho):
        filho.pai = self
        self.filhos.inserir_no_fim(filho)

    def remover_filho(self, filho):
        posicao = 0
        for atual in self.filhos:
            if atual == filho:
                return self.filhos.remover(posicao)
            posicao += 1

    @property
    def filhos_info(self):
        l = list()
        for atual in self.filhos:
            l.append(atual.info)
        return {"filhos": l} if l else {}

    @property
    def info(self):
        _info = {"dados": self.dados}
        _info.update(self.filhos_info)
        return _info


class Arvore:

     def __init__(self):
         self.raiz = None

     @property
     def info(self):
         if self.raiz:
             return self.raiz.info
