"""Fila  - Estrutura de Dados - considerando as características de Python."""


class Fila:
    def __init__(self):
        self._fila = list()

    def entrar(self, elemento):
        self._fila.append(elemento)

    def sair(self):
        return self._fila.pop(0)

    @property
    def is_empty(self):
        return len(self._fila) == 0

    def imprimir(self):
        for i in self._fila:
            print(i)

    @property
    def quantidade(self):
        return len(self._fila)
