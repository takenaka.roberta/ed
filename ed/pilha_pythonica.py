"""Pilha  - usando as características de Python."""


class Pilha:
    def __init__(self):
        self._pilha = list()

    def empilhar(self, elemento):
        self._pilha.append(elemento)

    def desempilhar(self):
        if len(self._pilha) > 0:
            return self._pilha.pop(-1)

    @property
    def is_empty(self):
        return len(self._pilha) == 0

    @property
    def topo(self):
        if not self.is_empty:
            return self._pilha[-1]
