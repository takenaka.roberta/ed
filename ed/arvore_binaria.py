"""Árvore Binária - Estrutura de Dados."""
"""
Casos de Uso de Árvore:
    Busca binária
    Ordenação
"""
import unicodedata

def normalizar_tupla(conteudo):
    items = []
    for item in conteudo:
        items.append(normalizar_string(item))
    return tuple(items)


def normalizar_string(conteudo):
    nkfd_form = unicodedata.normalize('NFKD', conteudo)
    only_ascii = nkfd_form.encode('ASCII', 'ignore')
    return only_ascii


def normalizar(conteudo):
    if isinstance(conteudo, str):
        return normalizar_string(conteudo)
    elif isinstance(conteudo, tuple):
        return normalizar_tupla(conteudo)
    return conteudo


class Node:
    def __init__(self, conteudo):
        self.conteudo = conteudo
        self.direita = None
        self.esquerda = None
        self.normalizado = normalizar(conteudo)

    @property
    def info(self):
        _info = {"valor": self.conteudo}
        if self.esquerda:
            _info.update({"{} (menores)".format(self.conteudo): self.esquerda.info})
        if self.direita:
            _info.update({"{} (maiores)".format(self.conteudo): self.direita.info})
        return _info

    def imprimir(self):
        if self.esquerda:
            self.esquerda.imprimir()
        print(self.conteudo)
        if self.direita:
            self.direita.imprimir()

    def inserir(self, elemento):
        if normalizar(elemento) < self.normalizado:
            if self.esquerda is None:
                self.esquerda = Node(elemento)
            else:
                self.esquerda.inserir(elemento)
        else:
            if self.direita is None:
                self.direita = Node(elemento)
            else:
                self.direita.inserir(elemento)

    def buscar(self, parent, elemento):
        _elemento = normalizar(elemento)
        if _elemento < self.normalizado:
            return self.esquerda.buscar(self, elemento)
        elif _elemento > self.normalizado:
            return self.direita.buscar(self, elemento)
        elif _elemento == self.normalizado:
            return parent, self

    def remover(self, arvore, parent):
        if self.esquerda and self.direita:
            self._remover_nodo_com_dois_filhos(arvore, parent)
        else:
            removido = self
            substituto = self.esquerda or self.direita
            if removido is arvore.raiz:
                arvore.raiz = substituto
            else:
                if parent.esquerda is self:
                    parent.esquerda = substituto
                else:
                    parent.direita = substituto
        removido.direita = removido.esquerda = None
        return removido

    def _remover_nodo_com_dois_filhos(self, arvore, parent):
        direita = self.direita
        esquerda = self.esquerda
        substituto = direita
        pai_substituto = self
        while substituto.esquerda:
            pai_substituto = substituto
            substituto = substituto.esquerda
        pai_substituto.esquerda = substituto.direita
        removido = self
        removido.esquerda = None
        removido.direita = None
        if parent is None:
            arvore.raiz = substituto
        else:
            parent.direita = substituto
        substituto.direita = direita
        substituto.esquerda = esquerda


class ArvoreBinaria:
    def __init__(self):
        self.raiz = None

    def imprimir(self):
        print("--- Arvore Binaria ---")
        self.raiz.imprimir()
        print("--- fim --")

    def inserir(self, elemento):
        if self.raiz is None:
            self.raiz = Node(elemento)
        else:
            self.raiz.inserir(elemento)

    def buscar(self, elemento):
        return self.raiz.buscar(None, elemento)

    def remover(self, elemento):
        parent, node = self.buscar(elemento)
        if node:
            node.remover(self, parent)


if __name__ == "__main__":
    arvore = ArvoreBinaria()
    arvore.inserir("Lasanha")
    arvore.imprimir()
    arvore.inserir("Arroz")
    arvore.inserir("Macarrão")
    arvore.inserir("Feijoada")
    arvore.inserir("Pizza")
    arvore.inserir("Churrasco")
    arvore.inserir("Frango")
    arvore.inserir("Peixe")
    arvore.inserir("Massas")
    arvore.inserir("Feijão")
    arvore.imprimir()
    item = arvore.buscar("Frango")
    print(item)
    arvore.remover("Pizza")
    arvore.imprimir()
    arvore.remover("Churrasco")
    arvore.imprimir()
