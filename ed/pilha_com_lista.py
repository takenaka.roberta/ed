"""Pilha  - usando as características de Python."""

from ed.lista_duplamente_ligada import ListaDuplamenteLigada


class Pilha:
    def __init__(self):
        self._pilha = ListaDuplamenteLigada()

    def empilhar(self, elemento):
        self._pilha.inserir_no_fim(elemento)

    def desempilhar(self):
        return self._pilha.remover_do_fim()

    @property
    def is_empty(self):
        return self._pilha.quantidade == 0

    @property
    def topo(self):
        if not self.is_empty:
            return self._pilha.fim
