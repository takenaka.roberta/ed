"""Lista encadeada com Python."""


class Celula:
    """Celula que compoe a lista."""

    def __init__(self, conteudo):
        """Inicializa Celula."""
        self.conteudo = conteudo
        self.proximo = None


class ListaLigada:
    """Lista ligada com Python."""

    def __init__(self):
        self._inicio = None
        self._quantidade = 0

    @property
    def quantidade(self):
        return self._quantidade

    def imprimir(self):
        print("-- Impressao da lista --")
        atual = self._inicio
        for i in range(0, self.quantidade):
            print(atual.conteudo)
            atual = atual.proximo
        print("-- fim --")

    @property
    def elementos(self):
        l = []
        item = self._inicio
        for i in range(0, self.quantidade):
            l.append(item.conteudo)
            item = item.proximo
        return l

    def inserir_no_inicio(self, conteudo):
        celula = Celula(conteudo)
        celula.proximo = self._inicio
        self._inicio = celula
        self._quantidade += 1

    def inserir_no_fim(self, conteudo):
        if self.quantidade == 0:
            return self.inserir_no_inicio(conteudo)
        # localizar o fim
        atual = self._inicio
        while atual and atual.proximo:
            atual = atual.proximo
        # criar uma celula
        celula = Celula(conteudo)
        atual.proximo = celula
        self._quantidade += 1

    def validar_posicao(self, posicao):
        if posicao < 0 or posicao > self._quantidade:
            raise ValueError(
                "Valor de posicao informado: {}. "
                "Mas valor esperado é de 0 a {}. ".format(posicao, self.quantidade)
            )
        return True

    def localizar_celula(self, posicao):
        self.validar_posicao(posicao)
        celula = self._inicio
        for i in range(0, posicao):
            celula = celula.proximo
        return celula

    def conteudo(self, posicao):
        atual = self.localizar_celula(posicao)
        return atual.conteudo

    def inserir(self, posicao, conteudo):
        if posicao == 0:
            return self.inserir_no_inicio(conteudo)
        if posicao == self.quantidade:
            return self.inserir_no_fim(conteudo)
        anterior = self.localizar_celula(posicao - 1)
        novo = Celula(conteudo)
        novo.proximo = anterior.proximo
        anterior.proximo = novo
        self._quantidade += 1

    def remover_do_inicio(self):
        removido = self._inicio
        self._inicio = self._inicio.proximo
        removido.proximo = None
        self._quantidade -= 1
        return removido

    def remover(self, posicao):
        if posicao == 0:
            return self.remover_do_inicio()
        anterior = self.localizar_celula(posicao - 1)
        removido = anterior.proximo
        anterior.proximo = removido.proximo
        removido.proximo = None
        self._quantidade -= 1
        return removido
