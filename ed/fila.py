"""Fila  - Estrutura de Dados - conceito - desconsiderando as características de Python e usando lista encadeada."""
from ed.lista_duplamente_ligada import ListaDuplamenteLigada


class Fila:
    def __init__(self):
        self._fila = ListaDuplamenteLigada()

    def entrar(self, elemento):
        self._fila.inserir(self._fila.quantidade, elemento)

    def sair(self):
        celula = self._fila.remover(0)
        return celula.conteudo

    @property
    def vazia(self):
        return self._fila.quantidade == 0

    def imprimir(self):
        self._fila.imprimir()

    @property
    def quantidade(self):
        return self._fila.quantidade
