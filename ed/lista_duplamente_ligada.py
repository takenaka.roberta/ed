"""Lista duplamente ligada com Python."""


class Celula:
    def __init__(self, conteudo):
        self._conteudo = conteudo
        self.proximo = None
        self.anterior = None

    def __str__(self):
        return self._conteudo

    def __eq__(self, conteudo):
        return conteudo == self._conteudo

    @property
    def conteudo(self):
        return self._conteudo


class ListaDuplamenteLigada:
    def __init__(self):
        self._inicio = None
        self._fim = None
        self._quantidade = 0

    def __iter__(self):
        item = self._inicio
        for i in range(0, self.quantidade):
            yield item.conteudo
            item = item.proximo

    @property
    def quantidade(self):
        return self._quantidade
    @property
    def inicio(self):
        return self._inicio
    @property
    def fim(self):
        return self._fim

    def imprimir(self):
        print("-- Impressao da lista --")
        atual = self._inicio
        for i in range(0, self.quantidade):
            print(atual.conteudo)
            atual = atual.proximo
        print("-- fim --")

    def validar_posicao(self, posicao):
        if 0 <= posicao <= self._quantidade:
            return True
        raise ValueError(
                "Valor de posicao informado: {}. "
                "Mas valor esperado é de 0 a {}. ".format(
                    posicao, self.quantidade
                )
            )

    def localizar_celula(self, posicao):
        self.validar_posicao(posicao)
        metade = int(self.quantidade / 2)
        if posicao < metade:
            celula = self._inicio
            for i in range(0, posicao):
                celula = celula.proximo
            return celula
        celula = self._fim
        for i in range(posicao, self.quantidade):
             celula = celula.anterior
        return celula

    def conteudo(self, posicao):
        item = self.localizar_celula(posicao)
        return item.conteudo

    def inserir_no_inicio(self, conteudo):
        celula = Celula(conteudo)
        celula.proximo = self._inicio
        celula.anterior = None
        if self._inicio:
            self._inicio.anterior = celula
        self._inicio = celula
        if self._quantidade == 0:
            self._fim = celula
        self._quantidade += 1

    def inserir_no_fim(self, conteudo):
        celula = Celula(conteudo)
        celula.proximo = None
        celula.anterior = self._fim
        if self._fim:
            self._fim.proximo = celula
        self._fim = celula
        if self._quantidade == 0:
            self._inicio = celula
        self._quantidade += 1

    def inserir(self, posicao, conteudo):
        self.validar_posicao(posicao)
        if posicao == 0:
            return self.inserir_no_inicio(conteudo)
        if posicao == self.quantidade:
            return self.inserir_no_fim(conteudo)
        anterior = self.localizar_celula(posicao-1)
        novo = Celula(conteudo)
        novo.proximo = anterior.proximo
        novo.anterior = anterior
        anterior.proximo = novo
        self._quantidade += 1

    def remover(self, posicao):
        removido = self.localizar_celula(posicao)
        if posicao == 0:
            return self.remover_do_inicio()
        if posicao == self._quantidade - 1:
            return self.remover_do_fim()
        anterior = removido.anterior
        proximo = removido.proximo
        anterior.proximo = proximo
        proximo.anterior = anterior
        removido.proximo = None
        removido.anterior = None
        self._quantidade -= 1
        return removido

    def remover_do_fim(self):
        if self.quantidade > 0:
            removido = self._fim
            self._fim = self._fim.anterior
            self._fim.proximo = None
            if self.quantidade == 1:
                self._inicio = None
            removido.proximo = None
            removido.anterior = None
            self._quantidade -= 1
            return removido

    def remover_do_inicio(self):
        if self.quantidade > 0:
            removido = self._inicio
            self._inicio = self._inicio.proximo
            self._inicio.anterior = None
            if self._quantidade == 1:
                self._fim = None
            removido.proximo = None
            self._quantidade -= 1
            return removido
