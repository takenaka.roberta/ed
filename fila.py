from ed.fila import Fila


class Pedido:
    def __init__(self, descricao):
        self.descricao = descricao

    def __repr__(self):
        return self.descricao


def executa_receber_e_processar():
    fila = Fila()
    fila.entrar(Pedido("Pizza Muçarela"))
    fila.entrar(Pedido("Pizza Marguerita"))
    fila.entrar(Pedido("Pizza Napolitana"))
    fila.entrar(Pedido("Pizza Escarola"))

    pizza = fila.sair()
    print(pizza)
    pizza = fila.sair()
    print(pizza)
    pizza = fila.sair()
    print(pizza)

    fila.entrar(Pedido("Pizza Calabreza"))
    fila.imprimir()


executa_receber_e_processar()
