"""
Casos de Uso de Árvore:
    Busca binária
    Ordenação
"""

from ed.arvore_binaria import ArvoreBinaria
import json

def registrar_letras(arvore):
    letras = "HGLBJTIKOZMPN"
    for l in letras:
        arvore.inserir(l)
    arvore.imprimir()
    app_json = json.dumps(arvore.raiz.info)
    with open("letras.json", "w") as f:
        f.write(app_json)

    arvore.remover("L")
    arvore.imprimir()
    app_json = json.dumps(arvore.raiz.info)
    with open("letras2.json", "w") as f:
        f.write(app_json)

    arvore.remover("H")
    arvore.imprimir()
    app_json = json.dumps(arvore.raiz.info)
    with open("letras3.json", "w") as f:
        f.write(app_json)


def registrar_universidades(arvore):

    arvore.inserir(("Espanha", "Barcelona", "Universitat Autònoma de Barcelona"))

    arvore.imprimir()
    arvore.inserir(("Itália", "Florença", "Università degli Studi di Firenze"))
    arvore.inserir(("Itália", "Bologna", "Università di Bologna"))
    arvore.inserir(("Itália", "Perugia", "Università degli Studi di Perugia"))
    arvore.inserir(("Itália", "Modena e Reggio Emilia", "Università degli Studi di Modena e Reggio Emilia"))
    arvore.inserir(("Dinamarca", "Odense ", "Syddansk Universitet"))

    arvore.imprimir()
    arvore.inserir(("Brasil", "Campinas", "Unicamp"))
    arvore.inserir(("Brasil", "São Paulo", "Universidade Federal de São Paulo"))
    arvore.inserir(("Brasil", "São Paulo", "Universidade de São Paulo"))
    arvore.inserir(("Corea do Sul", "Seoul", "고려대학교"))
    arvore.inserir(("Cabo Verde", "Praia", "Universidade de Cabo Verde"))

    arvore.inserir(("Suíça", "Lausanne", "Ecole Polytechnique Federale de Lausanne"))
    arvore.inserir(("Suécia", "Uppsala", "Uppsala universitet)"))

    arvore.imprimir()
    arvore.inserir(("Alemanha", "Tubinga", "Eberhard Karls Universität Tübingen"))
    arvore.inserir(("Turquia", "Istambul", "İstanbul Üniversitesi"))

    arvore.imprimir()
    arvore.inserir(("África do Sul", "Johanesburgo", "Universidade de Wirswatersrand"))
    arvore.inserir(("Uruguai", "Montevidéu", "Universidad ORT Uruguay"))
    arvore.inserir(("China", "Nanning", "广西大学,"))

    arvore.imprimir()
    arvore.inserir(("França", "Nice", "Université Nice Sophia Antipolis"))
    arvore.inserir(("África do Sul", "Cidade do Cabo", "Universidade Cidade do Cabo"))
    arvore.inserir(("Inglaterra", "Huddersfield", "University of Huddersfield"))

    arvore.imprimir()
    app_json = json.dumps(arvore.raiz.info)
    with open("universidades1.json", "w") as f:
        f.write(app_json)

    item = arvore.buscar(("Uruguai", "Montevidéu", "Universidad ORT Uruguay"))

    arvore.remover(("África do Sul", "Cidade do Cabo", "Universidade Cidade do Cabo"))
    arvore.imprimir()

    arvore.remover(("Suécia", "Uppsala", "Uppsala universitet)"))
    arvore.imprimir()

    app_json = json.dumps(arvore.raiz.info)
    with open("universidades2.json", "w") as f:
        f.write(app_json)

#registrar_universidades(ArvoreBinaria())
registrar_letras(ArvoreBinaria())
